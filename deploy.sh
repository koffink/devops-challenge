kubectl apply -f namespaces.yaml
kubectl apply -f app-be-dc.yaml
kubectl apply -f app-be-ingress.yaml
kubectl apply -f app-be-svc.yaml
kubectl apply -f app-fe-dc.yaml
kubectl apply -f app-fe-ingress.yaml
kubectl apply -f app-fe-svc.yaml
